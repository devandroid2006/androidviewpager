package keystore.devandroid.com.androidviewpager

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

//function extension for layout inflate
fun ViewGroup.inflate(res: Int): View {
    return LayoutInflater.from(context).inflate(res, this, false)
}

class BlankFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return container!!.inflate(R.layout.fragment_blank)
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                BlankFragment()

    }
}
