package keystore.devandroid.com.androidviewpager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

//use FragmentStatePagerAdapter if content is dynamic or else use FragmentPagerAdapter
class PagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        //return the fragment based on the condition if u need.
       return BlankFragment.newInstance()
    }

    override fun getCount(): Int {
        return 10//return from list size or fixed
    }

    override fun getPageTitle(position: Int): CharSequence? {

        //return the paget title if required form list of titles
        //list.get(posistion) retrun the title for the page
        return "Page "+position //hardcoded plase provide page title that u need
    }
}